# pvxs conda recipe

Home: https://github.com/mdavidsaver/pvxs

Recipe license: BSD 3-Clause

Summary: Library and a set of CLI utilities acting as PVAccess protocol client and/or server
